#!/usr/bin/env perl
use strict;
use warnings;

package Devs;

sub new {
    # get the first argument of the function
    my $class = shift;

    # this is an reference to an array
    my $names = shift;

    # Bless the reference to the classname
    bless $names, $class;
    return $names;
}

sub addDev {
    my( $self, $name ) = @_;
    push(@$self, $name);
}

sub printNames {
    my $self = shift;
    foreach my $dev (@$self) {
        print $dev->getName() . "\n";
    }
}

1;