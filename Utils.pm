#!/usr/bin/env perl
use strict;
use warnings;

package Utils;

sub error_string {
    my $msg = shift;
    return "Error: $msg \n";
}

1;