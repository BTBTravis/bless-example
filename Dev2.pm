#!/usr/bin/env perl
use strict;
use warnings;

require Utils;

package Dev2;
use parent 'DevBase';

# new is not special we can use seb load as constructor
sub load {
   my $class = shift;
   my %self = (
      _first_name => shift,
      _last_name  => shift,
      _fav_lang   => shift,
      _is_westwing  => shift,
   );

   my $self_ref = \%self;
   bless $self_ref, $class;
   return $self_ref;
}

1;