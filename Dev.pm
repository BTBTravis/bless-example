#!/usr/bin/env perl
use strict;
use warnings;

require Utils;

package Dev;
use parent 'DevBase';

sub new {
    # get the first argument of the function
    my $class = shift;

    # create annonomus hash and reference it into scalur $self
    my $self = {
        _first_name  => shift,
        _last_name   => shift,
        _fav_lang    => shift,
        _is_westwing => shift,
    };

    # Bless the reference to the classname
    bless $self, $class;
    return $self;
}


1;