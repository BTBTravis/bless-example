#!/usr/bin/env perl
use strict;
use warnings;

require Utils;

package DevBase;

sub getName {
    my( $self ) = @_;
    return "$self->{_first_name} $self->{_last_name}"
}

sub printSlogan {
    my( $self ) = @_;

    my $company_name = 'Company X';
    if ($self->{_is_westwing}) {
        $company_name = "Westwing";
    }
print "
Hello from ${company_name} developer $self->{_first_name} $self->{_last_name}.
My favorite language is: $self->{_fav_lang}

";
}

sub checkForFavLanguage {
    my( $self ) = @_;

    print "Checking for $self->{_fav_lang}\n";
    my $output = qx(which $self->{_fav_lang});
    die Utils::error_string("could not find $self->{_fav_lang} on system") unless ($? == 0);
    print "found $self->{_fav_lang} at ${output}\n";
}

1;