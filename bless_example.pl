#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long qw(GetOptions);

use Utils;
require Dev;
require Dev2;
require Devs;

# script arguments
my $first_name = '';
my $last_name = '';
my $fav_language = '';
my $is_westwing = 0;
my $help = 0;

GetOptions(
    'first_name=s' => \$first_name,
    'last_name=s' => \$last_name,
    'fav_language=s' => \$fav_language,
    'is_westwing' => \$is_westwing,
    'help' => \$help,
) or die("Error in command line arguments\n");

if ($help) {
    print "Perl OOP Example
usage:
--first_name travis  <> First Name
--last_name  shears  <> Last Name
--fav_language perl  <> Favoriate language
--is_westwing        <> Is Westwing Employee
--help               <> Print this message
\n";
    exit 0;
}

# hard require all script arguments
die(error_string("missing --first_name")) unless ($first_name ne '');
die(error_string("missing --last_name")) unless ($last_name ne '');
die(error_string("missing --fav_language")) unless ($fav_language ne '');

# curly brace annon hash ref example
# my $dev = new Dev($first_name, $last_name, $fav_language, $is_westwing);
# $dev->printSlogan();
# $dev->checkForFavLanguage();

# def hash and ref it example
# my $dev = load Dev2($first_name, $last_name, $fav_language, $is_westwing);
# $dev->printSlogan();
# $dev->checkForFavLanguage();

# my @initDevList = (
#     new Dev('bob', 'smith', 'node', 0),
#     new Dev('jim', 'jones', 'bash', 1),
#     new Dev('tim', 'tompson', 'perl', 1)
# );

# my $devs = new Devs(\@initDevList);
# my $dev = new Dev($first_name, $last_name, $fav_language, $is_westwing);
# $devs->addDev($dev);
# $devs->printNames();